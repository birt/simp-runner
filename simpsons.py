# launch in the SDK/tools dir as follows:
# monkeyrunner simpsons.py

# monkeyrunner without arguments gives you interactive shell
# Also, make sure openCV jar + .so(Linux) or .dll (Win) are in your Classpath
# Imshow.jar is 3rd party library from github to visualize images with java openCV bindings


# Imports the monkeyrunner modules used by this program
print("importing modules...")
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import os
import sys
from time import sleep
sys.path.append('opencv-246.jar')
from org.opencv.core import Core as core
from org.opencv.core import CvType, Point, Scalar
from org.opencv.imgproc import Imgproc as cv2
from org.opencv.highgui import Highgui as hg
from org.opencv.core import Mat
from com.atul.JavaOpenCV import Imshow
import java.lang
java.lang.System.loadLibrary('opencv_java246')
print("imports done!")



# Connects to the current device, returning a MonkeyDevice object
device = MonkeyRunner.waitForConnection()
package = 'com.ea.game.simpsons4_row'

# sets a variable with the name of an Activity in the package
activity = 'com.ea.simpsons.SimpsonsSplashScreen'

# sets the name of the component to start
runComponent = package + '/' + activity

# Runs the component
#print("Starting game..")
#device.startActivity(component=runComponent)
#device.press('KEYCODE_HOME',MonkeyDevice.DOWN_AND_UP)
#print('Sleeping to allow for startup time')
#sleep(80)

#print('Tapping to continue...')
#device.touch(50,50,MonkeyDevice.DOWN_AND_UP)
#print('Allow game to load...')
#sleep(20)

def pushpos(x):
    device.touch(int(x.x),int(x.y),MonkeyDevice.DOWN)
    sleep(0.5)
    device.touch(int(x.x),int(x.y),MonkeyDevice.UP)


counter = 0
print('Starting screenshot loop')
im = Imshow('Result')
while counter < 5:
    result = device.takeSnapshot()
    # Writes the screenshot to a file
    print('Writing screenshot')
    result.writeToFile('myproject/shot' + str(counter) +'.png','png')
    img_rgb = hg.imread('myproject/shot' + str(counter) +'.png',cv2.COLOR_BGR2GRAY)
    #img_gray = cv2.cvtColor(img_rgb, img_gray,cv2.COLOR_BGR2GRAY)
    template = hg.imread('myproject/dollarsign3.png',cv2.COLOR_BGR2GRAY)
    #w, h = template.shape[::-1]
#	result_cols =  img_rgb.cols() - template.cols() + 1
#	result_rows = img_rgb.rows() - template.rows() + 1

    result_cols = img_rgb.cols() - template.cols() + 1
    result_rows = img_rgb.rows() - template.rows() + 1
    result = Mat()
    result.create(result_cols, result_rows, CvType.CV_32FC1 )
    cv2.matchTemplate(img_rgb,template,result,cv2.TM_CCOEFF_NORMED)
#    core.normalize( result, result, 0, 1, core.NORM_MINMAX, -1, Mat() )

    matchLoc = core.minMaxLoc(result)
    maxVal = matchLoc.maxVal
    threshold = 0.55
    print("minval:" + str(matchLoc.minVal))
    print("maxval:" + str(matchLoc.maxVal))
    matchLoc = matchLoc.maxLoc

    print("x coordinaat:" + str(matchLoc.x))
    print("y coordinaat:" + str(matchLoc.y))
    if maxVal > threshold:
        print("Dollar sign detected...initiating device tap..")
        pushpos(matchLoc)
    else:
        print("Detected area below match threshold...ignoring & sleeping 30 seconds...")
        sleep(30)
    core.rectangle( img_rgb, matchLoc, Point( matchLoc.x + template.cols() , matchLoc.y + template.rows() ), Scalar(0, 2, 8, 0 ))
#    hg.imwrite('myproject/res'+ str(counter) +'.png',img_rgb)
    im.showImage(img_rgb)
 #   counter = counter + 1
 #   sleep(3)


